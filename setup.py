from setuptools import setup, find_packages
from os.path import join, dirname

import smart

setup(
    name = 'smart',
    version = smart.__version__,
    packages = find_packages(), install_requires = ['pathos'],
    long_description = open(join(dirname(__file__),'README.md')).read(),
    entry_points = {
        'console_scripts':[
            'smart-rm = smart.smart_rm: main',
            'smart-trash = smart.smart_trash: main'
            ]
        },
    test_suite='tests'
    )
