#-*-coding: utf-8-*-
import json
import os
import shutil
import unittest

import datetime

from smart.exceptions import ExistsException
from smart.smart_trash import restore_files

from smart.smart_rm import remove_files

from smart.bucket import Bucket
from smart.defaults import (
    BUCKET_PATH,
    SIZE_FILES_STORAGE,
    TIME_FILES_STORAGE,
    MAX_BUCKET_SIZE,
    VIEW_FILES_NUMBER,
    HISTORY_PATH,
    FILES_PATH,
    INFO_PATH, TRASHINFO)


class BucketTest(unittest.TestCase):

    def setUp(self):
        self.temp_dir = "temp_dir"
        os.mkdir(self.temp_dir)
        os.chdir(self.temp_dir)
        self.bucket = Bucket(
            bucket_path='bucket',
            bucket_files_path="bucket/files",
            bucket_info_path="bucket/info",
            view_files_number=20,
            history_path="bucket/history.log",
            time_files_storage=1,
            size_files_storage=1000
        )
        os.mkdir(self.bucket.bucket_path)
        os.mkdir(self.bucket.bucket_files_path)
        os.mkdir(self.bucket.bucket_info_path)


    def tearDown(self):
        os.chdir("..")
        shutil.rmtree(self.temp_dir)
        pass

    def test_create_bucket_object(self):
        bucket = Bucket()
        self.assertTrue(bucket.bucket_path is BUCKET_PATH)
        self.assertTrue(bucket.bucket_info_path is INFO_PATH)
        self.assertTrue(bucket.bucket_files_path is FILES_PATH)
        self.assertTrue(bucket.history_path is HISTORY_PATH)
        self.assertTrue(bucket.view_files_number is VIEW_FILES_NUMBER)
        self.assertTrue(bucket.max_bucket_size is MAX_BUCKET_SIZE)
        self.assertTrue(bucket.time_files_storage is TIME_FILES_STORAGE)
        self.assertTrue(bucket.size_files_storage is SIZE_FILES_STORAGE)

    def test_restore_file(self):
        file_name = "file"
        file_path = os.path.join(self.bucket.bucket_files_path, file_name)
        trashinfo_file_path = os.path.join(self.bucket.bucket_info_path, file_name + TRASHINFO)
        self.create_file_for_bucket(
            file_name,
            file_path,
            trashinfo_file_path
        )
        self.bucket.restore_file(file_name)
        self.assertTrue(os.path.exists(file_name))
        self.assertFalse(os.path.exists(file_path))
        self.assertFalse(os.path.exists(trashinfo_file_path))

    def test_restore_files(self):
        file_names = ['file_1','file_2','file_3','file_4','file_5']
        self.create_files_for_bucket(file_names)
        restore_files(bucket=self.bucket,files=file_names)
        for file_name in file_names:
            self.assertTrue(os.path.exists(file_name))
            file_path = os.path.join(self.bucket.bucket_files_path, file_name)
            trashinfo_file_path = os.path.join(self.bucket.bucket_info_path, file_name + TRASHINFO)
            self.assertFalse(os.path.exists(file_path))
            self.assertFalse(os.path.exists(trashinfo_file_path))


    def test_restore_nonexisting_file(self):
        file_name = "name"
        with self.assertRaises(ExistsException):
            self.bucket.restore_file(file_name)

    def test_clean_bucket(self):
        self.create_files_for_bucket(['file_1','file_2','file_3','file_4','file_5'])
        self.bucket.clean()
        self.assertTrue(os.path.exists(self.bucket.bucket_info_path))
        self.assertTrue(os.path.exists(self.bucket.bucket_files_path))
        self.assertFalse(os.listdir(self.bucket.bucket_info_path))
        self.assertFalse(os.listdir(self.bucket.bucket_files_path))


    def test_clean_by_time(self):
        file_names_for_cleaning = ['file_1', 'file_2', 'file_3', 'file_4', 'file_5']
        self.create_files_for_bucket(
            file_names=file_names_for_cleaning,
            date="2017-06-10 16:59:08"
        )
        saved_file_names = ['file_11', 'file_12', 'file_13', 'file_14', 'file_15']
        self.create_files_for_bucket(file_names=saved_file_names)
        self.bucket.clean_by_time()
        for file_name in file_names_for_cleaning:
            file_path = os.path.join(self.bucket.bucket_files_path, file_name)
            trashinfo_file_path = os.path.join(self.bucket.bucket_info_path, file_name + TRASHINFO)
            self.assertFalse(os.path.exists(file_path))
            self.assertFalse(os.path.exists(trashinfo_file_path))
        for file_name in saved_file_names:
            file_path = os.path.join(self.bucket.bucket_files_path, file_name)
            trashinfo_file_path = os.path.join(self.bucket.bucket_info_path, file_name + TRASHINFO)
            self.assertTrue(os.path.exists(file_path))
            self.assertTrue(os.path.exists(trashinfo_file_path))


    def test_clean_by_size(self):
        file_names_for_cleaning = ['file_1', 'file_2', 'file_3', 'file_4', 'file_5']
        self.create_files_for_bucket(
            file_names=file_names_for_cleaning,
            size=2000
        )
        saved_file_names = ['file_11', 'file_12', 'file_13', 'file_14', 'file_15']
        self.create_files_for_bucket(file_names=saved_file_names)
        self.bucket.clean_by_size()
        for file_name in file_names_for_cleaning:
            file_path = os.path.join(self.bucket.bucket_files_path, file_name)
            trashinfo_file_path = os.path.join(self.bucket.bucket_info_path, file_name + TRASHINFO)
            self.assertFalse(os.path.exists(file_path))
            self.assertFalse(os.path.exists(trashinfo_file_path))
        for file_name in saved_file_names:
            file_path = os.path.join(self.bucket.bucket_files_path, file_name)
            trashinfo_file_path = os.path.join(self.bucket.bucket_info_path, file_name + TRASHINFO)
            self.assertTrue(os.path.exists(file_path))
            self.assertTrue(os.path.exists(trashinfo_file_path))

    def test_get_contents(self):
        file_names = ['file_1','file_2','file_3','file_4','file_5']
        self.create_files_for_bucket(file_names)
        files_information = self.bucket.get_contents()
        self.assertIsInstance(file_names, list)
        for file_info in files_information:
            self.assertIsInstance(file_info, dict)

    def test_create_bucket(self):
        bucket = Bucket(
            bucket_path='BUCKET',
            bucket_files_path="BUCKET/files",
            bucket_info_path="BUCKET/info"
        )
        bucket.create_bucket()
        self.assertTrue(os.path.exists(bucket.bucket_path))
        self.assertTrue(os.path.exists(bucket.bucket_files_path))
        self.assertTrue(os.path.exists(bucket.bucket_info_path))

    def test_try_create_existing_bucket(self):
        file_names = ['file_1','file_2','file_3','file_4','file_5']
        self.create_files_for_bucket(file_names)
        bucket = Bucket(
            bucket_path='bucket',
            bucket_files_path="bucket/files",
            bucket_info_path="bucket/info"
        )
        bucket.create_bucket()
        self.assertTrue(os.path.exists(bucket.bucket_path))
        self.assertTrue(os.path.exists(bucket.bucket_files_path))
        self.assertTrue(os.path.exists(bucket.bucket_info_path))
        for file_name in file_names:
            file_path = os.path.join(self.bucket.bucket_files_path, file_name)
            trashinfo_file_path = os.path.join(self.bucket.bucket_info_path, file_name + TRASHINFO)
            self.assertTrue(os.path.exists(file_path))
            self.assertTrue(os.path.exists(trashinfo_file_path))

    def test_remove_wrong_files_from_bucket_files(self):
        file_names = ['file_1','file_2','file_3','file_4','file_5']
        self.create_files_for_bucket(file_names)
        file_names_for_cleaning = ["aaa","sss","ddd","fff","ggg"]
        for file_name in file_names_for_cleaning:
            file_path = os.path.join(self.bucket.bucket_files_path,file_name)
            open(file_path, "a").close()

        self.bucket.trash_auto_clean()

        for file_name in file_names_for_cleaning:
            file_path = os.path.join(self.bucket.bucket_files_path, file_name)
            trashinfo_file_path = os.path.join(self.bucket.bucket_info_path, file_name + TRASHINFO)
            self.assertFalse(os.path.exists(file_path))
            self.assertFalse(os.path.exists(trashinfo_file_path))
        for file_name in file_names:
            file_path = os.path.join(self.bucket.bucket_files_path, file_name)
            trashinfo_file_path = os.path.join(self.bucket.bucket_info_path, file_name + TRASHINFO)
            self.assertTrue(os.path.exists(file_path))
            self.assertTrue(os.path.exists(trashinfo_file_path))

    def test_remove_wrong_files_from_bucket_info(self):
        file_names = ['file_1','file_2','file_3','file_4','file_5']
        self.create_files_for_bucket(file_names)
        file_names_for_cleaning = ["aaa","sss","ddd","fff","ggg"]
        for file_name in file_names_for_cleaning:
            file_path = os.path.join(self.bucket.bucket_files_path,file_name+TRASHINFO)
            open(file_path, "a").close()

        self.bucket.trash_auto_clean()

        for file_name in file_names_for_cleaning:
            file_path = os.path.join(self.bucket.bucket_files_path, file_name)
            trashinfo_file_path = os.path.join(self.bucket.bucket_info_path, file_name + TRASHINFO)
            self.assertFalse(os.path.exists(file_path))
            self.assertFalse(os.path.exists(trashinfo_file_path))
        for file_name in file_names:
            file_path = os.path.join(self.bucket.bucket_files_path, file_name)
            trashinfo_file_path = os.path.join(self.bucket.bucket_info_path, file_name + TRASHINFO)
            self.assertTrue(os.path.exists(file_path))
            self.assertTrue(os.path.exists(trashinfo_file_path))

    def test_restore_file_with_used_name(self):
        file_name = "file"
        os.mkdir(file_name)
        file_path = os.path.join(self.bucket.bucket_files_path, file_name)
        trashinfo_file_path = os.path.join(self.bucket.bucket_info_path, file_name + TRASHINFO)
        self.create_file_for_bucket(
            file_name,
            file_path,
            trashinfo_file_path
        )
        self.bucket.restore_file(file_name)
        self.assertTrue(os.path.isdir(file_name))
        self.assertTrue(os.path.isfile(file_name+".1"))
        self.assertFalse(os.path.exists(file_path))
        self.assertFalse(os.path.exists(trashinfo_file_path))


    def create_files_for_bucket(
            self,
            file_names,
            size=None,
            date=None
    ):
        for file_name in file_names:
            file_path = os.path.join(self.bucket.bucket_files_path, file_name)
            trashinfo_file_path = os.path.join(self.bucket.bucket_info_path, file_name + TRASHINFO)
            self.create_file_for_bucket(
                file_name=file_name,
                file_path=file_path,
                trashinfo_file_path=trashinfo_file_path,
                size=size,
                date=date,
            )

    def create_file_for_bucket(
            self,
            file_name,
            file_path,
            trashinfo_file_path,
            date = None,
            size = None
    ):
        if not size:
            size = 100
        if not date:
            date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        open(file_path, "a").close()
        with open(trashinfo_file_path, "w") as file:
            json.dump(
                {
                    "Path": os.path.abspath(file_name),
                    "DeletionDate": date,
                    "Size": size
                },
                file
            )

if __name__ == '__main__':
    unittest.main()

