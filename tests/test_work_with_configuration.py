import os
import shutil
import unittest

from smart.config import Configuration, write_conf_config, write_json_config
from smart.defaults import (
    BUCKET_PATH,
    SIZE_FILES_STORAGE,
    TIME_FILES_STORAGE,
    MAX_BUCKET_SIZE,
    VIEW_FILES_NUMBER,
    HISTORY_PATH,
    FILES_PATH,
    INFO_PATH)
from smart.exceptions import WrongConfigFileExtension, WrongConfigFile
from smart.parser import create_remove_parser


class ConfigurationTest(unittest.TestCase):

    def setUp(self):
        self.temp_dir = "temp_dir"
        if not os.path.exists(self.temp_dir):
            os.mkdir(self.temp_dir)
        os.chdir(self.temp_dir)
        self.parser = create_remove_parser()
        self.config = {
            "bucket_path": "bucket",
            "bucket_files_path":"bucket/files",
            "bucket_info_path": "bucket/info",
            "history_path": "bucket/history.log",
            "max_bucket_size": 500,
            "view_files_number": 100,
            "time_files_storage": 100,
            "size_files_storage": 100,
        }

    def tearDown(self):
        os.chdir("..")
        shutil.rmtree(self.temp_dir)

    def test_create_default_configuration(self):
        configuration = Configuration()
        self.assertTrue(
            configuration.bucket_path is BUCKET_PATH and
            configuration.bucket_info_path is INFO_PATH and
            configuration.bucket_files_path is FILES_PATH and
            configuration.history_path is HISTORY_PATH and
            configuration.view_files_number is VIEW_FILES_NUMBER and
            configuration.max_bucket_size is MAX_BUCKET_SIZE and
            configuration.time_files_storage is TIME_FILES_STORAGE and
            configuration.size_files_storage is SIZE_FILES_STORAGE
        )

    def test_create_json_config(self):
        config_file = "conf.json"
        write_json_config(config_file=config_file, configuration=self.config)
        self.assertTrue(os.path.exists(config_file))

    def test_update_configuration_by_json_file(self):
        config_file="conf.json"
        write_json_config(config_file=config_file, configuration=self.config)
        arguments = self.parser.parse_args(["--config-file", "conf.json"])
        configuration = Configuration()
        configuration.update_configuration(args=arguments)
        self.assertTrue(configuration.bucket_path == self.config["bucket_path"])
        self.assertTrue(configuration.bucket_info_path == self.config["bucket_info_path"])
        self.assertTrue(configuration.bucket_files_path == self.config["bucket_files_path"])
        self.assertTrue(configuration.history_path == self.config["history_path"])
        self.assertTrue(configuration.view_files_number == self.config["view_files_number"])
        self.assertTrue(configuration.max_bucket_size == self.config["max_bucket_size"])
        self.assertTrue(configuration.time_files_storage == self.config["time_files_storage"])
        self.assertTrue(configuration.size_files_storage == self.config["size_files_storage"])

    def test_create_conf_config(self):
        config_file = "conf.conf"
        write_conf_config(config_file=config_file, configuration=self.config)
        self.assertTrue(os.path.exists(config_file))

    def test_update_configuration_by_conf_file(self):
        config_file="conf.conf"
        write_conf_config(config_file=config_file, configuration=self.config)
        arguments = self.parser.parse_args(["--config-file", "conf.conf"])
        configuration = Configuration()
        configuration.update_configuration(args=arguments)
        self.assertTrue(configuration.bucket_path == self.config["bucket_path"])
        self.assertTrue(configuration.bucket_info_path == self.config["bucket_info_path"])
        self.assertTrue(configuration.bucket_files_path == self.config["bucket_files_path"])
        self.assertTrue(configuration.history_path == self.config["history_path"])
        self.assertTrue(configuration.view_files_number == self.config["view_files_number"])
        self.assertTrue(configuration.max_bucket_size == self.config["max_bucket_size"])
        self.assertTrue(configuration.time_files_storage == self.config["time_files_storage"])
        self.assertTrue(configuration.size_files_storage == self.config["size_files_storage"])

    def test_update_configuration_wrong_format_file(self):
        config_file="conf.aaaaa"
        arguments = self.parser.parse_args(["--config-file", config_file])
        with self.assertRaises(WrongConfigFileExtension):
            configuration = Configuration()
            configuration.update_configuration(args=arguments)

    def test_update_configuration_wrong_json_file(self):
        config_file="aaa.json"
        with open(config_file, "w") as file:
            file.write("{aaaaaaaaaaaaaaaaaa}")
        arguments = self.parser.parse_args(["--config-file", config_file])
        configuration = Configuration()
        with self.assertRaises(WrongConfigFile):
            configuration.update_configuration(args=arguments)

    def test_update_configuration_wrong_conf_file(self):
        config_file="aaa.conf"
        with open(config_file, "w") as file:
            file.write("{aaaaaaaaaaaaaaaaaa}")
        arguments = self.parser.parse_args(["--config-file", config_file])
        configuration = Configuration()
        with self.assertRaises(WrongConfigFile):
            configuration.update_configuration(args=arguments)

    def test_update_by_particular_arguments(self):
        arguments = self.parser.parse_args(
            [
                "--bucket-path-policy", self.config["bucket_path"],
                "--view-count--policy", str(self.config["view_files_number"]),
                "--max-bucket-size", str(self.config["max_bucket_size"]),
                "--time-policy", str(self.config["time_files_storage"]),
                "--size-policy", str(self.config["size_files_storage"])
            ]
        )
        configuration = Configuration()
        configuration.update_configuration(args=arguments)
        self.assertTrue(configuration.bucket_path == self.config["bucket_path"])
        self.assertTrue(configuration.bucket_info_path == self.config["bucket_info_path"])
        self.assertTrue(configuration.bucket_files_path == self.config["bucket_files_path"])
        self.assertTrue(configuration.history_path == self.config["history_path"])
        self.assertTrue(configuration.view_files_number == self.config["view_files_number"])
        self.assertTrue(configuration.max_bucket_size == self.config["max_bucket_size"])
        self.assertTrue(configuration.time_files_storage == self.config["time_files_storage"])
        self.assertTrue(configuration.size_files_storage == self.config["size_files_storage"])


if __name__ == '__main__':
    unittest.main()