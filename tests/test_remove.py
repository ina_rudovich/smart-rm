# -*- encoding: utf-8 -*-

import os
import shutil
import unittest

from smart.defaults import TRASHINFO, HISTORY_PATH
from smart.file_utils import regular_expression_search, is_access_directory_mode
from smart.log import create_logger

from smart.remover import Remover
from smart.bucket import Bucket

import smart
import smart.parser
from smart.exceptions import (
    ExistsException,
    NotEmptyDirectory,
    IsDirElement,
    PermissionDenied,
    NotEnoughSpace,
    ElementFromBucket)


class RemoveTest(unittest.TestCase):

    def setUp(self):
        self.temp_dir = "temp_dir"
        if not os.path.exists(self.temp_dir):
            os.mkdir(self.temp_dir)
        os.chdir(self.temp_dir)
        self.bucket_path = "bucket"
        self.bucket_info_path = "bucket/info"
        self.bucket_files_path = "bucket/files"
        # self.history_path = "bucket/history"
        self.history_path = HISTORY_PATH

        self.remover = Remover(
            bucket_path=self.bucket_path,
            bucket_info_path=self.bucket_info_path,
            bucket_files_path=self.bucket_files_path,
            history_path=self.history_path,
            )

        self.bucket = Bucket(
            bucket_path=self.bucket_path,
            bucket_info_path=self.bucket_info_path,
            bucket_files_path=self.bucket_files_path,
            history_path=self.history_path,
        )
        self.bucket.prepare()

    def tearDown(self):
        for root, dirs, files in os.walk('.'):
            for dir in dirs:
                node_path = os.path.join(root, dir)
                os.chmod(node_path, 0777)
            for file in files:
                node_path = os.path.join(root, file)
                if not os.path.islink(node_path):
                    os.chmod(node_path, 0777)
        os.chdir("..")
        shutil.rmtree(self.temp_dir)
        pass

    def test_remove_simple_file(self):
        file = "file"
        open(file, "a").close()
        self.remover.remove_file(file=file)
        self.assertFalse(os.path.exists(file))

    def test_remove_file_without_mode(self):
        file = "file"
        open(file, "a").close()
        os.chmod(file, 0b000000100)
        with self.assertRaises(PermissionDenied):
            self.remover.remove_file(file=file)
        self.assertTrue(os.path.exists(file))

    def test_remove_file_in_dry_run_mode(self):
        file = "file"
        open(file, "a").close()
        self.remover.dry_run = True
        self.remover.remove_file(file=file)
        self.assertTrue(os.path.exists(file))

    def test_remove_empty_directory_without_flags(self):
        directory = "directory"
        os.mkdir(directory)
        with self.assertRaises(IsDirElement):
            self.remover.remove_dir(directory=directory)
        self.assertTrue(os.path.exists(directory))

    def test_remove_empty_directory_with_flag(self):
        directory = "directory"
        os.mkdir(directory)
        self.remover.dir_flag = True
        self.remover.remove_dir(directory=directory)
        self.assertFalse(os.path.exists(directory))

    def test_remove_notempty_directory_without_flags(self):
        directory = "directory"
        os.mkdir(directory)
        for i in range(0, 5):
            open("directory/file{}".format(i), "a").close()
        with self.assertRaises(IsDirElement):
            self.remover.remove_dir(directory=directory)
        self.assertTrue(os.path.exists(directory))

    def test_remove_notempty_directory_with_dir_flags(self):
        directory = "directory"
        os.mkdir(directory)
        for i in range(0, 5):
            open("directory/file{}".format(i), "a").close()
        self.remover.dir_flag = True
        with self.assertRaises(NotEmptyDirectory):
            self.remover.remove_dir(directory=directory)
        self.assertTrue(os.path.exists(directory))

    def test_remove_notempty_directory_with_recursive_flags(self):
        directory = "directory"
        os.mkdir(directory)
        for i in range(0, 5):
            open("directory/file{}".format(i), "a").close()
        self.remover.recursive_flag = True
        self.remover.remove_dir(directory=directory)
        self.assertFalse(os.path.exists(directory))

    def test_remove_directory_without_read_mode(self):
        directory = "directory"
        os.makedirs(directory)
        for i in range(0, 5):
            open("directory/file{}".format(i), "a").close()
        os.chmod(directory, 0333)
        self.remover.recursive_flag = True
        with self.assertRaises(PermissionDenied):
            self.remover.remove_dir(directory=directory)
        self.assertTrue(os.path.exists(directory))

    def test_remove_directory_without_read_mode_with_force_flag(self):
        directory = "directory"
        os.mkdir(directory)
        for i in range(0, 5):
            open("directory/file{}".format(i), "a").close()
        os.chmod(directory, 0333)
        self.remover.force = True
        self.remover.recursive_flag = True
        with self.assertRaises(PermissionDenied):
            self.remover.remove_dir(directory=directory)

    def test_regular_expression_search(self):
        directory = "directory"
        os.mkdir(directory)
        for i in range(0, 5):
            open("directory/file{}".format(i), "a").close()
        for i in range(0, 2):
            os.mkdir("directory/dir{}".format(i))
        for i in range(0, 3):
            open("directory/dir1/file{}".format(i), "a").close()
        files_from_regular_search = regular_expression_search(
            regular_expression="f[i-o]le*", directory=directory)
        correct_files = []
        for i in range(0, 5):
            correct_files.append(os.path.join(os.path.abspath("."), "directory/file{}".format(i)))
        for i in range(0, 3):
            correct_files.append(os.path.join(os.path.abspath("."), "directory/dir1/file{}".format(i)))
        self.assertEquals(set(files_from_regular_search), set(correct_files))

    def test_remove_symlink_on_existing_file(self):
        file = "file"
        link = "link1"
        open(file, "a").close()
        os.symlink(file, link)
        self.remover.remove_file(file=link)
        self.assertFalse(os.path.exists(link))
        self.assertTrue(os.path.exists(file))

    def test_remove_symlink_on_nonexisting_file(self):
        file = "file"
        link = "link2"
        open(file, "a").close()
        os.symlink(file, link)
        os.remove(file)
        self.assertFalse(os.path.exists(file))
        self.remover.remove_file(file=link)
        self.assertFalse(os.path.exists(link))

    def test_remove_nonexisting_file(self):
        with self.assertRaises(ExistsException):
            self.remover.remove_node(node="nonexisting_file")

    def test_remove_file_bigger_than_max_size(self):
        directory = "directory"
        os.mkdir(directory)
        self.remover.dir_flag = True,
        self.remover.max_bucket_size = 100
        with self.assertRaises(NotEnoughSpace):
            self.remover.remove_dir(directory=directory)

    # def test_remove_system_directory(self):
    #     directory = "/mnt"
    #     self.remover.recursive_flag = True
    #     self.remover.dry_run = True
    #     with self.assertRaises(PermissionDenied):
    #         self.remover.remove_dir(directory=directory)

    def test_access_mode(self):
        directory = "/mnt"
        a = is_access_directory_mode(directory)
        self.assertFalse(a)

    def test_replace_file_to_bucket(self):
        file = "file"
        open(file, "a").close()
        self.remover.remove_node(node=file)
        filepath_in_bucket = os.path.join(self.remover.bucket_files_path,file)
        self.assertTrue(os.path.exists(filepath_in_bucket))

    def test_create_trashinfo_file(self):
        file = "file"
        open(file, "a").close()
        self.remover.remove_node(node=file)
        trashinfo_filepath = os.path.join(self.remover.bucket_info_path,file+TRASHINFO)
        self.assertTrue(os.path.exists(trashinfo_filepath))

    def test_remove_bucket_file(self):
        print os.path.abspath(self.remover.bucket_path)
        with self.assertRaises(ElementFromBucket):
            self.remover.remove_node(self.bucket_files_path)

    def test_remove_files_with_same_names(self):
        for i in range(5):
            file = "file"
            open(file, "a").close()
            self.remover.remove_node(node=file)
        self.assertFalse(os.path.exists(file))
        filepath_in_bucket = os.path.join(self.bucket_files_path,file)
        self.assertTrue(os.path.exists(filepath_in_bucket))
        for i in range(1,4):
            self.assertTrue(os.path.exists("{path}.{n}".format(path=filepath_in_bucket, n=i)))
            trashinfo_file_path = os.path.join(self.bucket_info_path,file)
            self.assertTrue(os.path.exists("{path}.{n}{trasinfo}".format(path=trashinfo_file_path, n=i, trasinfo=TRASHINFO)))


if __name__ == '__main__':
    unittest.main()
