#-*-coding: utf-8-*-
"""This module ensures operation of the console application
for remove files to bucket"""
import errno
import os
import sys

from smart.bucket import Bucket
from smart.config import Configuration
from smart.exceptions import ExistsException
from smart.file_utils import regular_expression_search
from smart.log import create_logger
from smart.parser import create_remove_parser
from smart.remover import Remover

from pathos.multiprocessing import Pool

def main(unprocessed_arguments=sys.argv[1:]):
    """Main function to organize work of application of
    removing files

    Args:
        unprocessed_arguments(list): arguments of command line
    """
    parser = create_remove_parser()
    arguments = parser.parse_args(unprocessed_arguments)
    configuration = Configuration()
    logger = create_logger(
        history_path=configuration.history_path,
        module_name=__name__,
        dry_run=arguments.dry_run,
        silent=arguments.silent,
    )

    try:
        configuration.update_configuration(arguments)
    except ValueError as exception:
        logger.error(exception)

    bucket = Bucket(
        bucket_path=configuration.bucket_path,
        bucket_info_path=configuration.bucket_info_path,
        bucket_files_path=configuration.bucket_files_path,
        history_path=configuration.history_path,
        view_files_number=configuration.view_files_number,
        max_bucket_size=configuration.max_bucket_size,
        time_files_storage=configuration.time_files_storage,
        size_files_storage=configuration.size_files_storage,
        dry_run=arguments.dry_run,
    )
    bucket.prepare()

    remover = Remover(
        dry_run=arguments.dry_run,
        force=arguments.force,
        interactive=arguments.interactive,
        dir_flag=arguments.dir_flag,
        recursive_flag=arguments.recursive_flag,
        bucket_path=configuration.bucket_path,
        bucket_info_path=configuration.bucket_info_path,
        bucket_files_path=configuration.bucket_files_path,
        history_path=configuration.history_path,
        max_bucket_size=configuration.max_bucket_size,
    )
    process_remove_parameters(arguments=arguments, remover=remover, logger=logger)


def process_remove_parameters(
        arguments,
        remover,
        logger=None
    ):
    """Process remove parameters for correct remove.

    Args:
        arguments(argparse.Namespase): contains flags for removing
            and list of files for remove.
        remover(smart.remover.Remover): files remover
        logger(logging.RootLogger): object for logging information
    """
    if not remover:
        remover = Remover()

    if not logger:
        logger = create_logger()

    if arguments.force:
        arguments.interactive = False

    if arguments.regex:
        arguments.files.extend(
            regular_expression_search(
                directory=arguments.regex[1],
                regular_expression=arguments.regex[0]))

    exit_code = remove_files(
        files=arguments.files,
        remover=remover,
        logger=logger,
    )[1]

    raise SystemExit(exit_code)


def remove_files(
        remover,
        files,
        logger=None
    ):
    """Removes list of files

    Args:
        files(list(basestring)): list of files' paths for remove
        remover(smart.remover.Remover): files remover
        logger(logging.RootLogger): object for logging information
    Returns:
        list(dict): list with information about exceptions
        int: exit status of a process
    Raises:
        ExistsException: if no such file or directory
    """
    if not logger:
        logger = create_logger()

    def remove_file(file):
        try:
            if not (os.path.exists(file) or os.path.islink(file)):
                raise ExistsException(errno.ENOENT, "No such file or directory", file)

            if  os.path.isfile(file) or os.path.islink(file):
                remover.remove_file(file)
            else:
                remover.remove_dir(file)
            logger.info("remove file: "+file)
        except EnvironmentError as exception:
            logger.error(exception)
            return {
                    "errno": exception.errno,
                    "strerror": exception.strerror,
                    "file": exception.filename
                }
    
    pool = Pool()
    errors = pool.map(remove_file, files)
    pool.close()
    pool.join()
    errors = [error for error in errors if error is not None]
    if errors:
        exit_code = errors[-1]["errno"]
    else:
        exit_code = 0

    return errors, exit_code
