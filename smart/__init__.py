# -*- coding: utf-8 -*-
"""'Smart' package is improved version of 'rm' with support
bucket and ability to restore removed files an directories.

Provides:
    1.Class Configuration
    2.Class Bucket
    3.Class Remover

Example of usage:
    Create Configuration:
        configuration = Configuration()

        Configuration attributes:
            bucket_path(str): path to bucket.
            bucket_info_path(str): path to folder with files
                with real information about the past state of files
            bucket_files_path(str): path to folder, where removed
                files are stored.
            history_path(str): file path, which store history of
                action.
            view_files_number(int): count for files displayed for
                viewing.
            max_bucket_size(int): Max bucket size.
            time_files_storage(int): count day for clean bucket by
                time-policy.
            size_files_storage(int): size of file, value for clean
                by size_policy.


    Update Configuration():
        1)configuration.update_configuration(arguments)
        2)configuration.update_configuration_from_file('file')

    Create Remover:
        1)remover = smart.remover.Remover()
        Remover attributes:
            bucket_path(str): path to bucket.
            bucket_info_path(str): path to folder with files with
                real information about the past state of files
            bucket_files_path(str): path to folder, where removed
                files are staged.
            history_path(str): file path, which store history
                of action.
            max_bucket_size(int): Max bucket size.
            dry_run(bool): Flag shows in which mode Remover is used
                In dry_run mode nothing can be changed.
            force(bool): Flag show will Remover ignore the error
                or not.
            interactive(bool): Flag shows will Remover prompt
                before every removal or not.
            dir_flag(bool): File shows whether Remover can delete
                empty directories.
            recursive_flag(bool): File shows whether Remover can
                delete non-empty directories.

    Remove file:
        remover.remove_file('file1')

    Remove directory:
        remover.remove_dir('directory1')

    Remove list of files:
        smart.smart_rm_remove_files(
            files=['file1', 'dir1', 'link1', 'file2'],
            remover=remover
        )

    Create Bucket:
        bucket = smart.bucket.Bucket()

        Bucket attributes:
            bucket_path(str): path to bucket.
            bucket_info_path(str): path to folder with files with
                real information about the past state of files.
            bucket_files_path(str): path to folder, where removed
                files are storaged.
            history_path(str): file path, which store history
                of action.
            view_files_number(int): count for files for displaying
            max_bucket_size(int): Max bucket size.
            time_files_storage(int): count day for clean bucket by
                time-policy. If file lies in bucket longer than
                this value, it will be removed from bucket.
            size_files_storage(int): size of file. If file's size
                is bigger than this value, it will be removed from
                bucket.
            dry_run(bool): Flag shows in which mode bucket is used.
                In dry_run mode nothing can be changed.

    Prepare Bucket for work:
        bucket.prepare()

    Get information about files in bucket:
        contents = bucket.view_contents()

    Restore single file from bucket:
        bucket.restore_file('filename1')

    Restore files from bucket:
        smart.smart_trash.restored_files(
            files=['filename1', 'filename2','filename3'],
            bucket=bucket
        )
    """

__version__ = '1.0'
