#-*-coding: utf-8-*-
"""Default values for correct work of package"""
import os

BUCKET_PATH = os.path.join(os.path.expanduser("~"), "bucket")
INFO_PATH = os.path.join(os.path.expanduser("~"), "bucket", "info")
FILES_PATH = os.path.join(os.path.expanduser("~"), "bucket", "files")
HISTORY_PATH = os.path.join(os.path.expanduser("~"), "bucket", "history.log")
VIEW_FILES_NUMBER = 20
MAX_BUCKET_SIZE = 10000
TIME_FILES_STORAGE = 1
SIZE_FILES_STORAGE = 100

TRASHINFO = ".trashinfo"

WRITE_MODE_OK = 0b000010010
READ_MODE_OK = 0b000100100
