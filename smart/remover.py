#-*-coding: utf-8-*-
"""Module for work with remover"""
import datetime
import errno
import json
import os

from smart.file_utils import is_access_mode, enough_free_space_in_bucket, check_remove_from_folder

from smart.defaults import(
    BUCKET_PATH,
    INFO_PATH,
    FILES_PATH,
    HISTORY_PATH,
    MAX_BUCKET_SIZE,
    TRASHINFO,
    )
from smart.exceptions import(
    ExistsException,
    NotEmptyDirectory,
    IsDirElement,
    PermissionDenied,
    NotEnoughSpace,
    ElementFromBucket)
from smart.file_utils import get_new_filename_in_folder, get_node_size

def input_message(message):
    """Get string from standard input.

    Args:
        message: written to standard output.

    Returns:
        input string
    """
    return raw_input(message)


class Remover(object):
    """This class is intended for work with removing files.

    Attributes:
        bucket_path(basestring): store path to bucket.
        bucket_info_path(basestring): store path to folder with files with real information
            about the past state of files.
        bucket_files_path(basestring): store path to folder, where removed files are staged.
        history_path(basestring): file path, which store history of action.
        max_bucket_size(int): Max bucket size.
        dry_run(bool): Flag shows in which mode Remover is used. In dry_run mode nothing
            can be changed.
        force(bool): Flag show will someone ignore the error or not.
        interactive(bool): Flag shows will Remover prompt before every removal or not.
        dir_flag(bool): File shows whether Remover can delete empty directories.
        recursive_flag(bool): File shows whether Remover can delete non-empty directories.

    Args:
        bucket_path(basestring): store path to bucket.Defaults to smart.defaults.BUCKET_PATH.
        bucket_info_path(basestring): store path to folder with files with real information
            about the past state of files. Defaults to smart.defaults.INFO_PATH.
        bucket_files_path(basestring): store path to folder, where removed files are staged.
            Defaults to smart.defaults.FILES_PATH.
        history_path(basestring): file path, which store history of action.
            Defaults to smart.defaults.HISTORY_PATH.
        max_bucket_size(int): Max bucket size. Defaults to smart.defaults.MAX_BUCKET_SIZE.
        dry_run(bool): Flag shows in which mode Remover is used. In dry_run mode nothing
            can be changed. Defaults to False.
        force(bool): Flag show will someone ignore the error or not. Defaults to False.
        interactive(bool): Flag shows will Remover prompt before every removal or not.
            Defaults to False.
        dir_flag(bool): File shows whether Remover can delete empty directories.
            Defaults to False.
        recursive_flag(bool): File shows whether Remover can delete non-empty directories.
            Defaults to False.
    """

    def __init__(
            self,
            dry_run=False,
            force=False,
            interactive=False,
            dir_flag=False,
            recursive_flag=False,
            bucket_path=BUCKET_PATH,
            bucket_info_path=INFO_PATH,
            bucket_files_path=FILES_PATH,
            history_path=HISTORY_PATH,
            max_bucket_size=MAX_BUCKET_SIZE,
        ):
        self.dry_run = dry_run
        self.force = force
        self.interactive = interactive
        self.dir_flag = dir_flag
        self.recursive_flag = recursive_flag
        self.bucket_path = bucket_path
        self.bucket_info_path = bucket_info_path
        self.bucket_files_path = bucket_files_path
        self.history_path = history_path
        self.max_bucket_size = max_bucket_size


    def remove_file(
            self,
            file,
            ask=input_message,
        ):
        """Remove file.

            Args:
                file(basestring): path to file for remove.
                ask(function): function for asking in interactive mode.
                    Defaults to input_message.

            Returns:
                path, in which the file lies in 'bucket'

            Raises:
                TypeError: if 'file' isn't 'basestring'
        """
        if not isinstance(file, basestring):
            raise TypeError("Expected type 'basestring'")

        if self.interactive:
            answer = ask("remove file %s'?  "%file)
            if not (answer == "y" or answer == "yes"):
                return

        file_path_in_bucket = self.remove_node(file)
        return file_path_in_bucket


    def remove_dir(self, directory):
        """Remove directory.

        Args:
            directory(basestring): path to 'directory' for remove.

        Returns:
            path, in which the directory lies in 'bucket'

        Raises:
            TypeError: if 'file' isn't 'basestring'
        """
        if not isinstance(directory, basestring):
            raise TypeError("Expected type 'basestring'")

        if not os.access(directory, os.R_OK):
            raise PermissionDenied(errno.EACCES, "Permission denied", directory)

        if os.access(directory, os.R_OK) and os.listdir(directory):
            empty_dir = False
        else:
            empty_dir = True

        if not (self.recursive_flag  or self.dir_flag) and not self.force:
            raise IsDirElement(errno.EISDIR, "Is a directory", directory)

        if not (empty_dir or self.recursive_flag) and self.dir_flag and not self.force:
            raise NotEmptyDirectory(errno.ENOTEMPTY, "Directory not empty", directory)

        if self.interactive and not self.force:
            list_with_nodes_for_remove = Remover.recursive_folder_walk(directory)
            file_paths_in_bucket = []
            for node in list_with_nodes_for_remove:
                file_path_in_bucket = self.remove_node(node)
                file_paths_in_bucket.append(file_path_in_bucket)
            return file_paths_in_bucket

        file_path_in_bucket = self.remove_node(directory)

        return file_path_in_bucket


    def remove_node(self, node):
        """Remove node.

        Args:
            node(basestring): path to 'node' for remove.

        Returns:
            path, in which the node lies in 'bucket'

        Raises:
            TypeError: if 'file' isn't 'basestring'
            ExistsException: if no such file or directory
            NotEnoughSpace: if not enough space in bucket
            ElementFromBucket: if node lies in bucket
            PermissionDenied: if no access modes
        """
        if not isinstance(node, basestring):
            raise TypeError("Expected type 'basestring'")

        if not (os.path.exists(node) or os.path.islink(node)):
            raise ExistsException(errno.ENOENT, "No such file or directory", node)

        free_space = enough_free_space_in_bucket(
            node=node,
            bucket_files_path=self.bucket_files_path,
            max_bucket_size=self.max_bucket_size,
        )

        if not free_space:
            raise NotEnoughSpace(errno.ENOSPC, "Not enough space in bucket")

        if check_remove_from_folder(node=node, folder=self.bucket_path):
            raise ElementFromBucket(errno.EFAULT, "File lies in bucket", node)

        if not (is_access_mode(node) or os.path.islink(node)):
            raise PermissionDenied(errno.EACCES, "Permission denied", node)

        file_path_in_bucket = self._remove_node(node)

        return file_path_in_bucket


    def _remove_node(
            self,
            node,
        ):
        node = os.path.abspath(node)
        new_basename = get_new_filename_in_folder(node, self.bucket_files_path)
        file_path_in_bucket = os.path.join(
            self.bucket_files_path,
            os.path.basename(new_basename)
        )
        file_trashinfo_path = os.path.join(
            self.bucket_info_path,
            os.path.basename(new_basename) + TRASHINFO
        )

        if not self.dry_run:
            with open(file_trashinfo_path, "w") as file:
                json.dump(
                    {
                        "Path": os.path.abspath(node),
                        "DeletionDate": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        "Size": get_node_size(node)
                    },
                    file
                )
            os.rename(node, file_path_in_bucket)
        return file_path_in_bucket


    @staticmethod
    def recursive_folder_walk(
            dir,
            ask=input_message,
            general_del_nodes_list=[],
            recursion_start=True):
        """Recursive folder walk in interactive mode

        Args:
            dir(basestring): path for directory
            ask(function): function for asking in interactive mode.
            general_del_nodes_list(list): list of nodes for removing
            recursion_start(bool): is this directory start of recursion
                Defaults to True.

        Returns:
            nodes for remove in this directory
            (or this directory when everything in it was chosen)

        Raises:
            NotEmptyDirectory: trying to remove not empty directory
        """
        answer = ask("descend into directory '%s'?  "%dir)
        del_nodes_list = []
        if answer == "y" or answer == "yes":
            for node in os.listdir(dir):
                node_path = os.path.join(os.path.abspath(dir), node)
                if os.path.isfile(node_path) or os.path.islink(node_path):
                    answer = ask(" remove file '%s'?  " %node)
                    if answer == "y" or answer == "yes":
                        del_nodes_list.append(node)
                    continue
                else:
                    list_dir = Remover.recursive_folder_walk(
                        node_path,
                        general_del_nodes_list=general_del_nodes_list,
                        recursion_start=False)
                    answer = ask("remove directory '%s'" %node)
                    if answer == "y" or answer == "yes":
                        if len(list_dir) == len(os.listdir(node_path)):
                            del_nodes_list.append(node)
                        else:
                            #raise OSError("cannot remove %s: Directory not empty" %node)
                            raise NotEmptyDirectory(errno.ENOTEMPTY, "Directory not empty", node)
                    else:
                        general_del_nodes_list.extend(
                            [os.path.join(node_path, element) for element in list_dir]
                        )

        if recursion_start:
            answer = ask("remove directory '%s'" %dir)
            if answer == "y" or answer == "yes":
                if len(del_nodes_list) == len(os.listdir(dir)):
                    general_del_nodes_list.extend([dir])
                    return general_del_nodes_list
            general_del_nodes_list.extend(
                [os.path.join(node_path, element) for element in del_nodes_list]
            )
            return general_del_nodes_list
        return del_nodes_list
