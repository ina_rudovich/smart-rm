#-*-coding: utf-8-*-
"""This module contains operations for work with bucket."""

import datetime
import errno
import json
import os
import shutil

from smart.file_utils import get_new_filename_in_folder

from smart.defaults import (
    BUCKET_PATH,
    INFO_PATH,
    FILES_PATH,
    HISTORY_PATH,
    VIEW_FILES_NUMBER,
    MAX_BUCKET_SIZE,
    TIME_FILES_STORAGE,
    SIZE_FILES_STORAGE,
    TRASHINFO,
)
from smart.exceptions import ExistsException


class Bucket(object):
    """This class is intended for work with bucket.

    Attributes:
        bucket_path(basestring): store path to bucket.
        bucket_info_path(basestring): store path to folder with files with real information
            about the past state of files.
        bucket_files_path(basestring): store path to folder, where removed files are storaged.
        history_path(basestring): file path, which store history of action.
        view_files_number(int): count for files displayed when viewing.
        max_bucket_size(int): Max bucket size.
        time_files_storage(int): count day for clean bucket by time-policy. If file lies
            in bucket longer than this value, it will be removed from bucket.
        size_files_storage(int): size of file. If file's size is bigger than this value,
            it will be removed from bucket.
        dry_run(bool): Flag shows in which mode bucket is used. In dry_run mode nothing
            can be changed.

    Args:
        bucket_path(basestring): store path to bucket.Defaults to smart.defaults.BUCKET_PATH.
        bucket_info_path(basestring): store path to folder with files with real information
            about the past state of files. Defaults to smart.defaults.INFO_PATH.
        bucket_files_path(basestring): store path to folder, where removed files are staged.
            Defaults to smart.defaults.FILES_PATH.
        history_path(basestring): file path, which store history of action.
            Defaults to smart.defaults.HISTORY_PATH.
        view_files_number(int): count for files displayed when viewing.
            Defaults to smart.defaults.VIEW_FILES_NUMBER.
        max_bucket_size(int): Max bucket size. Defaults to smart.defaults.MAX_BUCKET_SIZE.
        time_files_storage(int): count day for clean bucket by time-policy. If file lies
            in bucket longer than this value, it will be removed from bucket.
            Defaults to smart.defaults.TIME_FILES_STORAGE..
        size_files_storage(int): size of file. If file's size is bigger than this value,
            it will be removed from bucket. Defaults to smart.defaults.TIME_SIZE_STORAGE.
        dry_run(bool): Flag shows in which mode bucket is used. In dry_run mode nothing
            can be changed. Defaults to False.
    """

    def __init__(
            self,
            bucket_path=BUCKET_PATH,
            bucket_info_path=INFO_PATH,
            bucket_files_path=FILES_PATH,
            history_path=HISTORY_PATH,
            view_files_number=VIEW_FILES_NUMBER,
            max_bucket_size=MAX_BUCKET_SIZE,
            time_files_storage=TIME_FILES_STORAGE,
            size_files_storage=SIZE_FILES_STORAGE,
            dry_run=False,
    ):
        self.bucket_path = bucket_path
        self.bucket_files_path = bucket_files_path
        self.bucket_info_path = bucket_info_path
        self.history_path = history_path
        self.view_files_number = view_files_number
        self.max_bucket_size = max_bucket_size
        self.time_files_storage = time_files_storage
        self.size_files_storage = size_files_storage
        self.dry_run = dry_run

    def get_contents(self):
        """ Get all information about contents of the bucket.

        Returns:
            list(dict): list of dict-objects:
                {
                    'NameInBucket': name of file in bucket,
                    'Path': path, where file was before is was removed,
                    'Size': size of file,
                    'DeletionDate': date, whet file was removed.
                }
        """
        contents = []
        files = os.listdir(self.bucket_files_path)

        for filename in files:
            trashinfo_file_path = os.path.join(
                self.bucket_info_path,
                os.path.basename(filename) + TRASHINFO)

            with open(trashinfo_file_path, "r") as trashinfo_file:
                file_info = json.load(trashinfo_file)

            file_info["NameInBucket"] = filename
            contents.append(file_info)

        return contents

    def restore_file(
            self,
            file_name,
    ):
        """Restore file from bucket. Moving the file along the path specified in trashinfo-file

        Args:
            file_name(basestring): name of file in bucket, which can be restored.

        Returns:
            path, in which the file lies

        Raises:
            ExistsException: if no such file in bucket
            TypeError: if file_name isn't basestring
        """
        if not isinstance(file_name, basestring):
            raise TypeError("Expected type 'basestring'")

        file_path = os.path.join(self.bucket_files_path, file_name)
        if not os.path.exists(file_path):
            raise ExistsException(errno.ENOENT, "No such file or directory", file_name)

        trashinfo_file_path = os.path.join(
            self.bucket_info_path,
            os.path.basename(file_name) + TRASHINFO)
        with open(trashinfo_file_path, "r") as file:
            file_info = json.load(file)
        file_home_path, file_basename = os.path.split(file_info["Path"])
        new_basename = get_new_filename_in_folder(
            file=file_basename,
            path=file_home_path
        )
        os.renames(
            file_path,
            os.path.join(file_home_path, new_basename)
        )
        if not os.path.exists(self.bucket_files_path):
            os.makedirs(self.bucket_files_path)
        os.remove(trashinfo_file_path)
        return file_info["Path"]

    def clean(self):
        """Remove all files from bucket. Make bucket empty"""
        if not self.dry_run:
            shutil.rmtree(self.bucket_files_path)
            shutil.rmtree(self.bucket_info_path)
            os.mkdir(self.bucket_files_path)
            os.mkdir(self.bucket_info_path)

    def clean_by_time(self):
        """ Clean bucket by time policy. All files which are stored longer
        than Bucket.time_files_storage will be removed.

        Returns:
            Count of files, which are removed
        """
        removed_files_count = 0
        today = datetime.datetime.now()
        for node in os.listdir(self.bucket_files_path):
            trashfinfo_filepath = os.path.join(
                self.bucket_info_path,
                os.path.basename(node) + TRASHINFO)
            with open(trashfinfo_filepath, "r") as file:
                file_info = json.load(file)

            remove_time = datetime.datetime.strptime(
                file_info["DeletionDate"],
                "%Y-%m-%d %H:%M:%S"
            )

            if (today - remove_time).days >= self.time_files_storage:
                self.remove_from_bucket(node)
                removed_files_count += 1
        return removed_files_count

    def clean_by_size(self):
        """ Clean bucket by size policy. All files which size is bigger
        than  Bucket.size_files_storaged will be removed from bucket.

        Returns:
            Count of files, which are removed
        """
        removed_files_count = 0
        for node in os.listdir(self.bucket_files_path):
            trashfinfo_filepath = os.path.join(
                self.bucket_info_path,
                os.path.basename(node) + TRASHINFO)
            with open(trashfinfo_filepath, "r") as file:
                file_info = json.load(file)
            if file_info["Size"] >= self.size_files_storage:
                self.remove_from_bucket(node)
                removed_files_count += 1
        return removed_files_count

    def remove_from_bucket(
            self,
            file,
    ):
        """Remove file and trashinfo-file from bucket.

        Args:
            file(basestring): name of file in bucket, which will be removed.

        Returns:
            path, in which the file was storedd in bucket

        Raises:
            TypeError: if file isn't 'basestring'
        """
        if not isinstance(file, basestring):
            raise TypeError("Expected type 'basestring'")

        file_path = os.path.join(
            self.bucket_files_path,
            file)
        self.remove_node_from_bucket_files(file_path)
        trashinfo_file_path = os.path.join(
            self.bucket_info_path,
            file + TRASHINFO)
        os.remove(trashinfo_file_path)
        return file_path

    def remove_node_from_bucket_files(
            self,
            file_path,
    ):
        """Remove file from 'bucket_files_path'.

        Args:
            file_path(basestring): name of file in bucket, which will be removed.

        Returns:
            path, in which the file was stored in bucket

        Raises:
            TypeError: if file_path isn't basestring
        """
        if not isinstance(file_path, basestring):
            raise TypeError("Expected type 'basestring'")

        if os.path.isfile(file_path) or os.path.islink(file_path):
            os.remove(file_path)
        else:
            shutil.rmtree(file_path)
        return file_path

    def trash_auto_clean(self):
        """Remove from bucket waste files. Leaves files' pairs "file" - "file.trashinfo"

        Returns:
            Bucket with valid files on it's paths
        """
        files_in_bucket = set(os.listdir(self.bucket_files_path))
        trashinfo_files = set(os.listdir(self.bucket_info_path))

        for filename in files_in_bucket:
            trashinfo_filename = "{file_name}{trashinfo}".format(
                file_name=filename, trashinfo=TRASHINFO
            )
            if not trashinfo_filename in trashinfo_files:
                file_path = os.path.join(self.bucket_files_path, filename)
                self.remove_node_from_bucket_files(file_path)

        for trashinfo_filename in trashinfo_files:
            trashfinfo_filepath = os.path.join(self.bucket_info_path, trashinfo_filename)
            if trashinfo_filename.endswith(TRASHINFO):
                filename = trashinfo_filename[:-len(TRASHINFO)]
                if not filename in files_in_bucket:
                    os.remove(trashfinfo_filepath)
            else:
                os.remove(trashfinfo_filepath)
        return self

    def create_bucket(self):
        """Create directories by the path's indicated in the bucket

        Returns:
            Bucket with existing directories, indicated in it.
        """
        if not os.path.exists(self.bucket_path):
            os.makedirs(self.bucket_path)
        if not os.path.exists(self.bucket_files_path):
            os.makedirs(self.bucket_files_path)
        if not os.path.exists(self.bucket_info_path):
            os.makedirs(self.bucket_info_path)
        return self

    def prepare(self):
        """Create directories for bucket. And check files in it for
        validity.

        Returns:
            Bucket which is prepared for next work.
        """
        self.create_bucket()

        self.trash_auto_clean()

        return self
