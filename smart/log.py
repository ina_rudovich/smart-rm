#-*-coding: utf-8-*-
"""This module contains work with logging"""

import logging
import sys
import os
from smart.defaults import HISTORY_PATH


def create_logger(
        module_name=None,
        history_path=HISTORY_PATH,
        silent=True,
        dry_run=True):

    """This function create logger with different handlers.

    Args:
        module_name(basestring): name of module, where function was called.
            Defaults to None.
        history_path(basestring): path to file, where actions and
            errors will be displayed. Defaults to defaults.HISTORY_PATH.
        silent(bool, optional): This parameter shows existence of
            logging.StreamHandler. (In silent mode we can't write into
            any stream). Defaults to True.
        dry_run(bool, optional): This parameter shows existence of
            logging.FileHandler. (In silent mode we can't write in file,
            because it's mode for command immitation). Defaults to True.

    Returns:
        logging.RootLogger: logger with the appropriate handlers.

    """

    logger = logging.getLogger(module_name)
    logger.setLevel(logging.INFO)

    if not dry_run:
        file_handler = logging.FileHandler(history_path, "a")
        formatter = logging.Formatter(
            '%(asctime)s  %(levelname)s in %(module)s : %(message)s', '%Y-%m-%d %H:%M:%S'
        )
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)

    if not silent:
        stream_handler = logging.StreamHandler(sys.stdout)
        formatter = logging.Formatter('%(message)s')
        stream_handler.setFormatter(formatter)
        logger.addHandler(stream_handler)

    return logger
