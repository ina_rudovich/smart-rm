#-*-coding: utf-8-*-
"""This module contains different exceptions,
that may arise during the use of the package"""
class ExistsException(EnvironmentError):
    """File doesn't exist"""
    pass


class NotEmptyDirectory(EnvironmentError):
    """Directory isn't empty"""
    pass


class IsDirElement(EnvironmentError):
    """Element is Directory"""
    pass


class PermissionDenied(EnvironmentError):
    """No permission to access the file"""
    pass


class NotEnoughSpace(EnvironmentError):
    """Not enough space in the folder"""
    pass


class WrongConfigFileExtension(ValueError):
    """Inappropriate file extension"""
    pass


class WrongConfigFile(ValueError):
    """Inappropriate file contents"""
    pass


class ElementFromBucket(EnvironmentError):
    """File belongs to bucket"""
    pass

class NotDirectory(EnvironmentError):
    """File isn't directory"""