#-*-coding: utf-8-*-
""" Module for work with configuration"""
import argparse
import json
import os
from smart.exceptions import WrongConfigFile, WrongConfigFileExtension
from smart.log import create_logger
from smart.defaults import(
    BUCKET_PATH,
    INFO_PATH,
    FILES_PATH,
    HISTORY_PATH,
    VIEW_FILES_NUMBER,
    MAX_BUCKET_SIZE,
    TIME_FILES_STORAGE,
    SIZE_FILES_STORAGE,
    TRASHINFO,
    )

class Configuration(object):
    """Configuration, which contains various settings and parameters for correct work
        other objects. Can be update.

    Attributes:
        bucket_path(basestring): store path to bucket.
        bucket_info_path(basestring): store path to folder with files with real information
            about the past state of files.
        bucket_files_path(basestring): store path to folder, where removed files are storaged.
        history_path(basestring): file path, which store history of action.
        view_files_number(basestring): count for files displayed when viewing.
        max_bucket_size(int): Max bucket size.
        time_files_storage(int): count day for clean bucket by time-policy.
        size_files_storage(int): size of file, value for clean by size_policy.

    Args:
        bucket_path(basestring): store path to bucket.Defaults to smart.defaults.BUCKET_PATH.
        bucket_info_path(basestring): store path to folder with files with real information
            about the past state of files. Defaults to smart.defaults.INFO_PATH.
        bucket_files_path(basestring): store path to folder, where removed files are staged.
            Defaults to smart.defaults.FILES_PATH.
        history_path(basestring): file path, which store history of action.
            Defaults to smart.defaults.HISTORY_PATH.
        view_files_number(int): count for files displayed when viewing.
            Defaults to smart.defaults.VIEW_FILES_NUMBER.
        max_bucket_size(int): Max bucket size. Defaults to smart.defaults.MAX_BUCKET_SIZE.
        time_files_storage(int): count day for clean bucket by time-policy.
            Defaults to smart.defaults.TIME_FILES_STORAGE..
        size_files_storage(int): size of file, value for clean by size_policy.
            Defaults to smart.defaults.TIME_SIZE_STORAGE.
    """
    def __init__(
            self,
            bucket_path=BUCKET_PATH,
            bucket_info_path=INFO_PATH,
            bucket_files_path=FILES_PATH,
            history_path=HISTORY_PATH,
            view_files_number=VIEW_FILES_NUMBER,
            max_bucket_size=MAX_BUCKET_SIZE,
            time_files_storage=TIME_FILES_STORAGE,
            size_files_storage=SIZE_FILES_STORAGE,
        ):
        self.bucket_path = bucket_path
        self.bucket_files_path = bucket_files_path
        self.bucket_info_path = bucket_info_path
        self.history_path = history_path
        self.view_files_number = view_files_number
        self.max_bucket_size = max_bucket_size
        self.time_files_storage = time_files_storage
        self.size_files_storage = size_files_storage

    def update_configuration(self, args=None):
        """Update configuration from parameters of argparse.Namespace object

        Args:
            args(argparse.Namespase): contains parameters for update

        Returns:
            Updated configuration

        Raises:
            TypeError: if args isn't 'argparse.Namespace'
        """
        if not isinstance(args, argparse.Namespace):
            raise TypeError("Expected type 'argparse.Namespace'")
        if not args:
            pass

        if args.config_file:
            self.update_configuration_from_file(args.config_file)

        if args.bucket_path:
            self.bucket_path = args.bucket_path
            self.bucket_info_path = os.path.join(args.bucket_path, "info")
            self.bucket_files_path = os.path.join(args.bucket_path, "files")
            self.history_path = os.path.join(args.bucket_path, "history.log")

        if args.view_count:
            self.view_files_number = args.view_count

        if args.max_size:
            self.max_bucket_size = int(args.max_size)

        if args.clean_by_time and (args.clean_by_time is not TIME_FILES_STORAGE):
            self.time_files_storage = args.clean_by_time

        if args.clean_by_time and args.clean_by_size is not SIZE_FILES_STORAGE:
            self.size_files_storage = args.clean_by_size


    def update_configuration_from_file(self, config_file):
        """Update configuration from file

        Args:
            config_file(basestring): file path for update configuration

        Returns:
            Updated configuration

        Raises:
            TypeError: if args isn't 'basestring'
            WrongConfigFileExtension: if config file isn't *.json of *.config file
        """
        if not isinstance(config_file, basestring):
            raise TypeError("Expected type 'basestring'")

        if config_file.endswith(".json"):
            self.update_configuration_from_json_file(config_file)
        elif config_file.endswith(".conf"):
            self.update_configuration_from_conf_file(config_file)
        else:
            raise WrongConfigFileExtension("Wrong config-file format : %s" % (config_file))
        return self


    def update_configuration_from_json_file(self, config_file):
        """Update configuration from "*.json" file

        Args:
            config_file(basestring): file path for update configuration

        Returns:
            Updated configuration

        Raises:
            TypeError: if args isn't 'basestring'
            ValueError: if config_file doesn't exists or it's not "*.json"
            WrongConfigFile: if file content doesn't in json
        """
        if not isinstance(config_file, basestring):
            raise TypeError("Expected type 'basestring'")
        if not (os.path.exists(config_file) and config_file.endswith(".json")):
            raise ValueError("Expected existing '*.json' file")

        with open(config_file, "r") as file:
            stream = file.read()
            try:
                user_config = json.loads(stream)
            except ValueError as exception:
                raise WrongConfigFile("Wrong configuration file : %s" %(config_file))

            if not isinstance(user_config, dict):
                raise WrongConfigFile("Wrong configuration file : %s" %(config_file))
        self._extend_dict(user_config)

        return self


    def update_configuration_from_conf_file(self, config_file):
        """Update configuration from "*.conf" file

        Args:
            config_file(basestring): file path for update configuration

        Returns:
            Updated configuration

        Raises:
            TypeError: if args isn't 'basestring'
            ValueError: if config_file doesn't exists or it's not "*.conf"
            WrongConfigFile: if file content doesn't in special format
        """
        if not isinstance(config_file, basestring):
            raise TypeError("Expected type 'basestring'")
        if not (os.path.exists(config_file) and config_file.endswith(".conf")):
            raise ValueError("Expected existing '*.conf' file")

        with open(config_file, "r") as file:
            user_config = {}
            try:
                for line in file.readlines():
                    key, value = line.rstrip("\n").split(" = ")
                    user_config[key] = value
            except ValueError as e:
                raise WrongConfigFile("Wrong configuration file : %s" %(config_file))
        self._extend_dict(user_config)
        return self

    def _extend_dict(self, user_config={}):
        """Update configuration from dict

        Args:
            user_config: dict with parameters of configuration

        Returns:
            Updated configuration
        """
        self.bucket_path = user_config.get(
            "bucket_path",
            self.bucket_path)
        self.bucket_files_path = user_config.get(
            "bucket_files_path",
            self.bucket_files_path)
        self.bucket_info_path = user_config.get(
            "bucket_info_path",
            self.bucket_info_path)
        self.history_path = user_config.get(
            "history_path",
            self.history_path)
        self.max_bucket_size = int(user_config.get(
            "max_bucket_size",
            self.max_bucket_size))
        self.view_files_number = int(user_config.get(
            "view_files_number",
            self.view_files_number))
        self.time_files_storage = int(user_config.get(
            "time_files_storage",
            self.time_files_storage))
        self.size_files_storage = int(user_config.get(
            "size_files_storage",
            self.size_files_storage))
        return self


def write_json_config(config_file, configuration):
    """Write to file configuration in json format.

    Args:
        config_file(basestring): file for write
        configuration(dict): configuration for writing
    Raises:
        TypeError: if config_file isn't 'basestring' or configuration isn't 'dict'
    """
    if not isinstance(config_file, basestring):
        raise TypeError("Expected type 'basestring' for config_file")
    if not isinstance(configuration, dict):
        raise TypeError("Expected type 'dict' configuration")

    with open(config_file, "w") as file:
        json.dump(configuration, file, indent=4)


    #conf - my format
def write_conf_config(config_file, configuration):
    """Write to file configuration in format "key = value".

    Args:
        config_file(basestring): file for write
        configuration(dict): configuration for writing
    Raises:
        TypeError: if config_file isn't 'basestring' or configuration isn't 'dict'
    """
    if not isinstance(config_file, basestring):
        raise TypeError("Expected type 'basestring' for config_file")
    if not isinstance(configuration, dict):
        raise TypeError("Expected type 'dict' configuration")

    with open(config_file, "w") as file:
        for key, value in configuration.items():
            file.write("%s = %s\n"%(key, value))
