#-*-coding: utf-8-*-
"""This module ensures operation of the console application
for work with bucket"""
import sys
from math import ceil

from smart.bucket import Bucket
from smart.config import Configuration
from smart.defaults import VIEW_FILES_NUMBER
from smart.log import create_logger
from smart.parser import create_bucket_parser
from pathos.multiprocessing import Pool


def main(unprocessed_arguments=sys.argv[1:]):
    """Main function to organize application work with trash
    and bucket

    Args:
        unprocessed_arguments(list): arguments of command line
    """
    parser = create_bucket_parser()
    arguments = parser.parse_args(unprocessed_arguments)
    logger = None
    configuration = Configuration()
    try:
        configuration.update_configuration(arguments)
    except ValueError as exception:
        logger = create_logger(
            history_path=configuration.history_path,
            module_name=__name__,
            dry_run=arguments.dry_run,
            silent=arguments.silent,
        )
        logger.error(exception)

    if not logger:
        logger = create_logger(
            history_path=configuration.history_path,
            module_name=__name__,
            dry_run=arguments.dry_run,
            silent=arguments.silent,
        )

    bucket = Bucket(
        bucket_path=configuration.bucket_path,
        bucket_info_path=configuration.bucket_info_path,
        bucket_files_path=configuration.bucket_files_path,
        history_path=configuration.history_path,
        view_files_number=configuration.view_files_number,
        max_bucket_size=configuration.max_bucket_size,
        time_files_storage=configuration.time_files_storage,
        size_files_storage=configuration.size_files_storage,
        dry_run=arguments.dry_run,
    )
    bucket.prepare()

    exit_code = 0

    if arguments.look:
        logger.info("View the contents of the bucket")
        view_contents(
            bucket=bucket,
            view_count=bucket.view_files_number,
        )

    if arguments.restore:
        exit_code = restore_files(
            files=arguments.files,
            bucket=bucket,
            logger=logger,
        )[1]

    if arguments.clean:
        bucket.clean()
        logger.info("Clean bucket")

    if arguments.clean_by_time:
        bucket.clean_by_time()
        logger.info("Clean bucket by time")

    if arguments.clean_by_size:
        bucket.clean_by_size()
        logger.info("Clean bucket by size")

    raise SystemExit(exit_code)


def restore_files(
        bucket,
        files,
        logger=None
    ):
    """Restore list of files

    Args:
        files(list(str)): list of files' paths for restore
        bucket(smart.bucket.Bucket): bucket storage
        logger(logging.RootLogger): object for logging information
    Returns:
        list(dict): list with information about exceptions
        int: exit status of a process
    """
    if not logger:
        logger = create_logger()

    def restore_file(filename):
        try:
            if not bucket.dry_run:
                bucket.restore_file(filename)
            logger.info("restore file: " + filename)
        except EnvironmentError as exception:
            logger.error(exception)
            return {
                    "errno": exception.errno,
                    "strerror": exception.strerror,
                    "file": exception.filename
                }

    pool = Pool()
    errors = pool.map(restore_file, files)
    pool.close()
    pool.join()
    errors = [error for error in errors if error]
    if errors:
        exit_code = errors[-1]["errno"]
    else:
        exit_code = 0

    return errors, exit_code


def view_contents(
        bucket,
        view_count=VIEW_FILES_NUMBER,
        input_smth=None,
        output_smth=None,
    ):
    """Print information about contents of the bucket.

    Args:
        bucket(smart.bucket.Bucket): bucket storage
        view_count(int): length of information list's part for output
            Defaults to smart.defaults
        output_smth(function): function for output information
        input_smth(function): function for input information

    Returns:
        list(dict): list of dict-objects:
            {
                'NameInBucket': name of file in bucket,
                'Path': path, where file was before is was removed,
                'Size': size of file,
                'DeletionDate': date, whet file was removed.
            }
    """
    contents = bucket.get_contents()
    if not input_smth:
        input_smth = enter_smth
    if not output_smth:
        output_smth = print_smth
    number_of_groups = int(ceil(len(contents)/float(view_count)))
    output_smth(' + %+70s | %-30s + ' % ("-"*70, "-"*30))
    output_smth(' | %+70s | %-30s | ' % ("HOME FILE PATH", "NAME IN BUCKET"))
    output_smth(' + %+70s | %-30s + ' % ("-"*70, "-"*30))
    for i in range(0, number_of_groups):
        for file_info in contents[view_count * i : view_count * (i+1)]:
            output_smth(' | %+70s | %-30s | ' % (file_info["Path"], file_info["NameInBucket"]))
        if i is not number_of_groups-1:
            input_smth("press enter:")
    output_smth(' + %+70s | %-30s + ' % ("-"*70, "-"*30))
    return contents


def print_smth(message):
    """Print message at console.

    Args:
        message: message for print.
    """
    print message


def enter_smth(message):
    """Get string from standard input.
    
    Args:
        message: written to standard output.
    """
    return raw_input(message)
