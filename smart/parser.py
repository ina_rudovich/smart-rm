#-*-coding: utf-8-*-
"""This module contains functions for create parsers for
processed arguments"""

import argparse
import sys

from smart.defaults import (
    TIME_FILES_STORAGE,
    SIZE_FILES_STORAGE
)

def create_remove_parser():
    """This function parse argument when you need to processed arguments
    for remove.
    """
    parser = argparse.ArgumentParser(
        description="Remove the files")
    parser.add_argument(
        "-i", "--interactive",
        help="prompt before every removal",
        action="store_true")
    parser.add_argument(
        "-f", "--force",
        help="ignore nonexistent files and arguments, never prompt",
        action="store_true")
    parser.add_argument(
        "-r", "-R", "--recursive",
        dest="recursive_flag",
        help="remove directories and their contents recursively",
        action="store_true")
    parser.add_argument(
        "-d", "--dir",
        dest="dir_flag",
        help="remove empty directories",
        action="store_true")
    parser.add_argument(
        "files",
        metavar="FILE",
        nargs="*")
    parser.add_argument(
        "--regex",
        metavar=("regular_expression", "node"),
        help="parse regular expresions",
        nargs=2)
    parser.add_argument(
        "--silent",
        help="silent mode",
        action="store_true")
    parser.add_argument(
        "--dry-run",
        dest="dry_run",
        help="commands imitation",
        action="store_true")

    parser = add_config_flags(parser)

    return parser


def create_bucket_parser():
    """This function parse argument when you need to processed arguments
    for work with bucket.
    """
    parser = argparse.ArgumentParser(
        description="Temporary storage for files that have been deleted ",
        usage="[OPTION]... [FILE]...")

    parser.add_argument(
        "-l", "--look",
        help="view a list of files in bucket",
        action="store_true")
    parser.add_argument(
        "-r", "--restore",
        help="restore file",
        action="store_true")
    parser.add_argument(
        "-c", "--clean",
        help="clean bucket",
        action="store_true")
    parser.add_argument(
        "files",
        metavar="FILE",
        nargs="*")
    parser.add_argument(
        "--silent",
        help="silent mode",
        action="store_true")
    parser.add_argument(
        "--dry-run",
        dest="dry_run",
        help="command imitation",
        action="store_true")

    parser = add_config_flags(parser)
    return parser


def add_config_flags(parser):
    """This function add arguments for configuration to arguments parser.

    Args:
        parser(argparse.ArgumentParser): parser for update by configuration arguments.

    Returns:
        argparse.ArgumentParser: update parser.
    """
    parser.add_argument(
        "--config-file",
        dest="config_file",
        help="get configuration from file(*.json or *.conf)",
        type=str)
    parser.add_argument(
        "--bucket-path-policy",
        dest="bucket_path",
        help="set bucket path",
        )
    parser.add_argument(
        "--view-count--policy",
        dest="view_count",
        help="Count of files for look",
        type=int,
        )
    parser.add_argument(
        "--max-bucket-size",
        help="Set max bucket size",
        dest="max_size",
        type=int,
        )
    parser.add_argument(
        "-t", "--time-policy",
        dest="clean_by_time",
        help="time policy for bucket cleaning(count of days from deletion)",
        type=int,
        nargs="?",
        const=TIME_FILES_STORAGE,
        )
    parser.add_argument(
        "-s", "--size-policy",
        dest="clean_by_size",
        help="size policy for bucket cleaning(size in bytes)",
        type=int,
        nargs="?",
        const=SIZE_FILES_STORAGE,
        )

    return parser
