#-*-coding: utf-8-*-
"""This module contain different functions for work with files"""
import os
import re

# from multiprocessing import Pool
from multiprocessing import Manager

import errno
import stat

from smart.defaults import FILES_PATH, MAX_BUCKET_SIZE, READ_MODE_OK, WRITE_MODE_OK
from smart.exceptions import ExistsException, NotDirectory
from pathos.multiprocessing import Pool


def get_new_filename_in_folder(
        file,
        path,
):
    """Check file with such name in directory(path).
    If such name exists det new basename for file

    Args:
        file(basestring): file, which basename will be checked
        path(basestring): path to directory, where will be checked

    Returns:
        new basename for file

    Raises:
        TypeError: if file or path isn't 'basestring' or 'unicode'
        NotDirectory: if by path 'path' lies not directory
    """
    if not os.path.exists(path):
        raise ExistsException(errno.ENOENT, "No such file or directory", path)
    if not os.path.isdir(path):
        raise NotDirectory(errno.ENOTDIR, "Not a directory", path)
    basename = os.path.basename(file)
    if basename not in os.listdir(path):
        return file

    index = 0
    for item in os.listdir(path):
        if re.search(r"^%s(.\d+)?$" % basename, item):
            index_str = re.search(r"(.\d+)?$", item).group()
            if index_str:
                index = max(int(index_str[1:]), index)
    index += 1
    return file + "." + str(index)


def is_access_mode(node):
    """Check rights for node of all types
    Args:
        node(basestring): path to file, link or directory

    Returns:
        bool: True if node has access rights or False if there are not

    Raises:
        TypeError: if 'node' isn't 'basestring'
        ExistsException: if no such file by path 'node'
    """
    if not isinstance(node, basestring):
        raise TypeError("Expected type 'basestring' for 'node'")

    if os.path.islink(node):
        return True
    elif os.path.isfile(node):
        return _access_write_mode(node)
    elif os.path.isdir(node):
        return is_access_directory_mode(node)
    else:
        raise ExistsException(errno.ENOENT, "No such file or directory", node)


def is_access_directory_mode(directory):
    """Check access modes in directory.

    Args:
        directory(basestring): directory path for check.

    Returns:
        bool: True if all in directory has modes and False if they are not
    """
    for root, dirs, files in os.walk(directory):
        if not (_access_write_mode(root) and _access_read_mode(root)):
            return False
        for dir in dirs:
            node_path = os.path.join(root, dir)
            if not (_access_write_mode(node_path) and _access_read_mode(node_path)):
                return False
        for file in files:
            node_path = os.path.join(root, file)
            if not _access_write_mode(node_path):
                return False
    return True


def _access_write_mode(file):
    return stat.S_IMODE(os.stat(file).st_mode) & WRITE_MODE_OK


def _access_read_mode(file):
    return stat.S_IMODE(os.stat(file).st_mode) & READ_MODE_OK


def get_node_size(node):
    """Get size for 'node' of all types.

    Args:
        node(basestring): path to file, link or directory

    Returns:
        size of node
    """
    if not os.path.exists(node):
        return 0
    elif os.path.isfile(node) or os.path.islink(node):
        return _get_file_size(node)
    else:
        return _get_dir_size(node)


def _get_file_size(file):
    return os.path.getsize(file)


def _get_dir_size(directory):
    size = 0
    for root, dirs, files in os.walk(directory):
        size += os.path.getsize(directory)
        for dir in dirs:
            size += os.path.getsize(os.path.join(root, dir))
        for file in files:
            size += os.path.getsize(os.path.join(root, file))
    return size


def regular_expression_search(
        directory, regular_expression):
    """Find files in 'directory', which are correspond
    to 'regular_expression'

    Args:
        directory(basestring): path for directory, in which search will be
        regular_expression(basestring): string for regular expression

    Returns:
        list(basestring)
            to 'regular_expression'

    Raises:
        ExistsException: if 'directory' doesn't exists
    """
    if not os.path.exists(directory):
        raise ExistsException(errno.ENOENT, "No such file or directory", directory)

    pattern = re.compile(regular_expression)
    re_nodes_list = Manager().list()
    nodes_stack = [os.path.abspath(directory)]
    pool = Pool(10)
    nodes = Manager().list()

    def pattern_search(node):
        if pattern.search(os.path.basename(node)):
            re_nodes_list.append(node)
        elif os.path.isdir(node) and not os.path.islink(node):
            nodes.append(node)

    while nodes_stack:
        pool.map(pattern_search, nodes_stack)
        nodes_stack = Manager().list()
        for node in nodes:
            try:
                nodes_stack.extend([os.path.join(node, file) for file in os.listdir(node)])
            except:
                pass
                
        nodes = Manager().list()

    pool.close()
    pool.join()
    return re_nodes_list


def enough_free_space_in_bucket(
        node,
        bucket_files_path=FILES_PATH,
        max_bucket_size=MAX_BUCKET_SIZE):
    """Check if in 'bucket_files_path' is enough free space to
     put 'node' in it.

    Args:
        node(basestring): path to node, which may be put in folder
        bucket_files_path(basestring): path to folder, which is storage for files
            Defaults to smart.defaults.FILES_PATH
        max_bucket_size(int): max size of folder.
            Defaults to smart.defaults.MAX_BUCKET_SIZE

    Returns:
        bool: True if there is enough space in folder for 'node', else - False
    """
    node_size = get_node_size(node)
    bucket_size = get_node_size(bucket_files_path)
    free_space = max_bucket_size - bucket_size
    return free_space > node_size


def check_remove_from_folder(node, folder):
    """Check whether the node belongs to a folder

    Args:
        node(basestring): path to node for check
        folder(basestring): path to folder for check

    Returns:
        bool: True if node belongs to folder, else - False
    """
    i = 0
    j = 0
    node_abspath = os.path.abspath(node)
    folder_abspath = os.path.abspath(folder)
    while i < len(node_abspath) and j < len(folder_abspath):
        if node_abspath[i] == folder_abspath[j]:
            i += 1
            j += 1
        else:
            break
    result = j == len(folder_abspath)
    return result
