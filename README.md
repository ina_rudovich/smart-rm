# smart-rm bash utility #
===========================

SmartRM package is improved version of the utility 'rm' with the possibility of placing files in the bucket.



## smart-rm mode ##
---------------------

Move files to bucket:
```shell
$ smart-rm file1 file2 file3
```

Move files to bucket with settings, specified in user config file:
```shell
$ smart-rm filename1 filename2 --config-file user_config_file
```

Remove files or symlinks:
```shell
$ smart-rm file1
$ smart-rm symlink_file1
```

Remove empty directory:
```shell
$ smart-rm -d directory1
$ smart-rm --directory directory1
```

Move not empty directory to trash bin:
```shell
$ smart-rm -r directory1
$ smart-rm --recursive directory1
```

Remove file in force mode:
```shell
$ smart-rm -f file1
$ smart-rm --force file1
```


### smart-trash mode ###
--------------------------

View the contents of the bucket:
```shell
$ smart-trash -l
$ smart-trash --look
```

Restore files from bucket with trash:
```shell
$ smart-trash -r filename1 filename2
$ smart-trash --recover filename1 filename2
```

Permanently remove files from bucket:
```shell
$ smart-trash -c filename1 filename2
$ smart-trash --clean filename1 filename2
```

Remove files which lie in bucket more than 'time_files_storage'(if there is argument, than
cunfiguration parameter 'time_files_storage' will be update):
```shell
$ smart-trash -t
$ smart-trash -t 5
$ smart-trash --time-policy
$ smart-trash --time-policy 5
```

Remove files which size is more than 'size_files_storage'(if there is argument, than
configuration parameter 'size_files_storage' will be update):
```shell
$ smart-trash -s
$ smart-trash -s 5
$ smart-trash --size-policy
$ smart-trash --size-policy 5
```


## Other program flags ##
--------------------------

###Modes###

#### Silent-Mode ####
Run program without any promts and printing current processes
Flag: ```--silent```

#### Dry-Run ####
Run program without any changes. Show only preliminary program result
Flag: ```--dry-run```


### Configuration flags ###

These flags should be used to set different options of configuration.

Update default configuration from file(*.json or *.conf):
Flag:
```shell
$ smart-rm --config-file config.json
$ smart-trash --config-file config.conf
```

Update "max_bucket_size" option(in bytes):
Flag:
```shell
$ smart-rm --max-bucket-size 100000
$ smart-trash --max-bucket-size 100000
```

Update "max_bucket_size" option(in bytes):
Flag:
```shell
$ smart-rm --bucket-path-policy path_to_bucket
$ smart-trash --bucket-path-policy path_to_bucket
```

Update "view_files_number" option(in bytes):
Flag:
```shell
$ smart-trash --view-count--policy 30
```

Update "view_files_number" option(in bytes):
Flag:
```shell
$ smart-trash --view-count--policy 30
```

Update "view_files_number" option(in bytes):
Flag:
```shell
$ smart-trash --view-count--policy 30
```
